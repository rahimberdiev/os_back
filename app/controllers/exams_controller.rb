class ExamsController < SecureController
  def list
    section = Section.find(params[:section_id])
    no_ids = section.exams.map{|q| {no:q.no,id:q.id}}
    no_gs = no_ids.group_by{|no_id| no_id[:no]}
    rnd_ids = no_gs.keys.map{|no_g| no_gs[no_g][rand(1..no_gs[no_g].length)-1][:id]}
    exams = rnd_ids.map{|rnd_id| section.exams.find{|ex| ex.id == rnd_id}}

    render json: exams.sort_by{|ex| ex.no || 0}.map{|q| {
      no:q.no,
      id:q.id, 
      question: q.question, 
      isformula: q.isformula,
      details: q.details,
      score:q.score,
      options: q.answers.order(:value).map{|o| {id: o.id, value: o.value, isformula:o.isformula}}, 
      pics: q.pics.order(:title).map{|pic| {
        title: pic.title,
        img: pic.img.url(:medium)
      }}
    }}
  end
  def check
    section = Section.includes(:exams).find(params[:section_id])
    exams = section.exams.map{|ex| {q: ex, ans: ex.answers.find_by(correct: true)}}

    points = exams.sum{|ex| 
      params[:work].find{|w| w[:id]==ex[:q].id && ex[:ans] && (w[:ans]==ex[:ans].id || w[:ans]==ex[:ans].id.to_s)} ? ex[:q].score: 0
    }
    max_points = exams.sum{|ex| 
      params[:work].find{|w| w[:id]==ex[:q].id} ? ex[:q].score: 0
    }

    score = max_points==0 ? 0 : (points*100)/max_points;
    passed = score >= (section.q_pass || 75)

    if passed
      section_progress = current_user.sectionProgresses.find_by(section_id: section.id)
      section_progress = current_user.sectionProgresses.new(section_id: section.id) if !section_progress
      section_progress.score = score
      section_progress.save()
    end


    render json: {
      score: score, 
      passed: passed
    }
  end
end
