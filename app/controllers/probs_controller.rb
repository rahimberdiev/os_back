class ProbsController < SecureController
  def probs_list
    testings = Testing.all
    render json: {
      list: testings.map{|testing| {
        id: testing.id,
        name: testing.name, 
        description: testing.description,
        duration: testing.duration,
      }},
    }
  end
  def list
    testing = Testing.find(params[:testing_id])
    render json: {
      list: testing.probs.order(:no).map{|prob| {
        no:prob.no,
        id:prob.id, 
        question: prob.question, 
        details: prob.details, 
        isformula: prob.isformula,
        isinstruction: prob.isinstruction,
        options: prob.opts.order(:value).map{|o| {id: o.id, value: o.value, isformula:o.isformula,crt: o.correct}}, 
        img: prob.img.present? ? prob.img.url(:medium) : ''
      }},
      duration: testing.duration,
      name:testing.name
    }
  end
  def check
    testing = Testing.find(params[:testing_id])

    probs = testing.probs.map{|prob| {p: prob, correct: prob.opts.find_by(correct: true)}}

    points = probs.sum{|ex| 
      params[:work].find{|w| w[:id]==ex[:p].id && ex[:correct] && (w[:ans]==ex[:correct].id || w[:ans]==ex[:correct].id.to_s)} ? 1 : 0
    }
    max_points = probs.sum{|ex| 
      params[:work].find{|w| w[:id]==ex[:p].id} ? 1 : 0
    }

    score = max_points==0 ? 0 : (points*100)/max_points;
    passed = score >= 75

    render json: {
      score: score, 
      passed: passed
    }
  end
end
