class ProfileController < SecureController
  def read
    render json: {fullname: current_user.fullname, photo: current_user.photo.url(:medium)}
  end

  def write
  end

  def save_avatar 
    current_user.photo = params[:avatar]
    current_user.save
    current_user.fullname=current_user.photo.url(:thumb)
    current_user.save
    render json: {
      photo: current_user.photo.url(:thumb),
    }
  end
end
