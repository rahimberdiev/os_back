class QuizzesController < SecureController
  def list
    sc = Screencast.find(params[:sc_id])
    no_ids = sc.quizzes.map{|q| {no:q.no,id:q.id}}
    no_gs = no_ids.group_by{|no_id| no_id[:no]}
    rnd_ids = no_gs.keys.map{|no_g| no_gs[no_g][rand(1..no_gs[no_g].length)-1][:id]}
    quizzes = rnd_ids.map{|rnd_id| sc.quizzes.find{|quiz| quiz.id == rnd_id}}

    render json: quizzes.sort_by{|ex| ex.no || 0}.map{|q| {
      id:q.id, 
      no:q.no, 
      question: q.question, 
      isformula: q.isformula,
      details: q.details, 
      score:q.score,
      options: q.options.order(:value).map{|o| {id: o.id, value: o.value, isformula:o.isformula}}, 
      imgs: q.imgs.order(:title).map{|img| {
        title: img.title,
        img: img.img.url(:medium)
      }}
    }}
  end
  def check
    sc = Screencast.includes(:quizzes).find(params[:sc_id])
    quizzes = sc.quizzes.map{|quiz| {q: quiz, ans: quiz.options.find_by(correct: true)}}

    points = quizzes.sum{|quiz| 
      params[:work].find{|w| w[:id]==quiz[:q].id && quiz[:ans] && (w[:ans]==quiz[:ans].id || w[:ans]==quiz[:ans].id.to_s)} ? (quiz[:q].score || 1): 0
    }
    max_points = quizzes.sum{|quiz| 
      params[:work].find{|w| w[:id]==quiz[:q].id} ? (quiz[:q].score || 1): 0
    }

    score = max_points==0 ? 0 : (points*100)/max_points;

    render json: {
      score: score,
    }
  end
end
