class RegionsController < ApplicationController
  def list
    render json: Region.order(:name).map{|region| {
      id:region.id,
      name: region.name,
      cities: region.schools.order(:name).map{|school| {id: school.id,name: school.name}}
    }}
  end
end
