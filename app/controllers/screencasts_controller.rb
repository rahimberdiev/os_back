class ScreencastsController < SecureController
  def list
    section= Section.find(params[:section_id])
    userCourse = UserCourse.find_by(user_id: current_user.id,course_id: section.course_id)
    if(!section)
      render json:{message:(current_user.lang==0 ? "Бөлүм табылган жок" : "Раздел не найден")},status:501 and return
    end
    render json: {
      list: section.screencasts.order(:no).map{|sc| {
        id:sc.id,
        no: sc.no,
        name: sc.name,
        logo:sc.logo.url(:thumb),
        video: (userCourse || sc.free ? sc.video.url.gsub("mp4","mp4.m3u8"): ""),
        header: sc.header,
        footer: sc.footer,
        free:sc.free
      }},
      course: {id: section.course.id,name:section.course.name}
    }
  end
  def activate 
    section = Section.find_by(id: params[:section_id])
    if(!section)
      render json:{message:(current_user.lang==0 ? "Бөлүм табылган жок" : "Раздел не найден")},status:501 and return
    end
    sc = section.screencasts.find{|sc| params[:sc_id]==-1 || sc.id == params[:sc_id]}

    if(!sc)
      render json:{message:(current_user.lang==0 ? "Сабак табылган жок же учурдагы бөлүмгө караштуу эмес" : "Урок не найден либо не относится к текущей части")},status:501 and return 
    end

    progress = current_user.courseProgresses.find_by(course_id:section.course_id,section_id:section.id)
    if !progress 
      render json:{message:(current_user.lang==0 ? "Сабак табылган жок же учурдагы бөлүмгө караштуу эмес" : "Урок не найден либо не относится к текущей части")},status:501 and return 
    end


    progress.sc_id=sc.id
    progress.save
    render json: {
      active_id: progress.section_id, active_sc_id: progress.sc_id
    }
  end

end
