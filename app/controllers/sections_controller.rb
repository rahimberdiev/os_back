class SectionsController < SecureController
  def list
    course = Course.includes(:sections).find(params[:course_id])
    if(!course)
      render json:{message:(current_user.lang==0 ? "Курс табылган жок" : "Курс не найден")},status:501 and return
    end
    if(course.sections.size==0)
      render json:{message:(current_user.lang==0 ? "курс даяр эмес" : "Курс не настроен")},status:501 and return
    end
    progress = current_user.courseProgresses.find_by(course_id:params[:course_id])

    if !progress
      section = course.sections.order(:no).first
      sc=section.screencasts.order(:no).first

      if(!sc)
        render json:{message:(current_user.lang==0 ? "Сабак табылган жок" : "Урок не найден")}, status:501 and return
      end

      progress = current_user.courseProgresses.create(course_id:params[:course_id],section_id:section.id,sc_id:sc.id)
      refreshRedis(progress,section)
    else 
      section = course.sections.find(progress.section_id)
      refreshRedis(progress,section)
    end

    userCourse = UserCourse.find_by(user_id: current_user.id,course_id: course.id)
    section_progresses = current_user.sectionProgresses.map{|progress| {section_id: progress.section_id,score: progress.score}}


    render json: {
      list:course.sections.order(:no).map{|section| {
        id:section.id,
        no:section.no,
        name: section.name,
        description:section.description,
        pass_score: section.q_pass, 
        score: section_progresses.find{|progress| progress[:section_id]==section.id},
        scs:section.screencasts.order(:no).map{|sc|{
          id:sc.id,
          no: sc.no,
          name: sc.name,
          logo:sc.logo.url(:thumb),
          video: (userCourse || sc.free ? sc.video.url.gsub("mp4","mp4.m3u8"): ""),
          header: sc.header,
          footer: sc.footer,
          free:sc.free
        }}
      }},
      active_id: progress.section_id,
      active_sc_id:progress.sc_id
    }
  end

  def move
    course = Course.find(params[:course_id])
    progress = current_user.courseProgresses.find_by(course_id:params[:course_id])
    userCourse = UserCourse.find_by(user_id: current_user.id,course_id: course.id)
    
    if !progress
      render :current and return 
    end 

    section_progress = current_user.sectionProgresses.find_by(section_id: progress.section_id)
    if !section_progress
      render json: {
        active_id: progress.section_id,
        active_sc_id:progress.sc_id
      } and return 
    end 


    section_no = Section.find(progress.section_id).no
    next_section = course.sections.order(:no).find{|section| section.no>section_no}
    sc=next_section.screencasts.order(:no).first

    if(!sc)
      render json:{message:(current_user.lang==0 ? "Сабак табылган жок" : "Урок не найден")}, status:501 and return
    end
    if(!sc.free && !userCourse)
      render json:{message:(current_user.lang==0 ? "Бөлүм чектелген" : "К разделу доступ ограничен")}, status:501 and return
    end

    progress.section_id = next_section.id
    progress.sc_id = sc.id
    progress.save
    refreshRedis(progress,next_section)
    render json: {
      active_id: progress.section_id,
      active_sc_id:progress.sc_id
    }
  end

  private

  def refreshRedis(progress,section)
    if(progress&&progress.section_id)
      redis = Redis.new
      redis.keys("sc:#{current_user.token}:*").each do |key|
        redis.del(key)
      end
      if !progress.completed
        section.screencasts.each do |sc| 
          redis.set("sc:#{current_user.token}:#{sc.id}",1)
        end
      end
    end
  end
end
