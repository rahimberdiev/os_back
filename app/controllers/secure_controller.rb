class SecureController < ApplicationController
  before_action :require_login
  def require_login
    authenticate_token || render_unauthorized("Access denied")
  end

  def current_user
    @current_user ||= authenticate_token
  end

  protected 

  def render_unauthorized(message)
    error = { errors: [{ details: message}]}
    render json: error, status: :unauthorized
  end

  private 

  def authenticate_token
    authenticate_with_http_token do |token, options|
      user = User.find_by(token: token)
      if !user 
        return nil
      end
      minutes = (Time.now - user.token_created_at)/10.minute
      if(minutes>60*24) 
        return nil
      end
      
      user.token_created_at = Time.now
      user.save

      return user

    end
  end  
end
