class SessionsController < SecureController
  skip_before_action :require_login, only: [:create,:send_pin,:change_password,:approve], raise: false
  def refresh_session 
    send_auth_token_for_valid_login_of(current_user)
  end

  def create
    if user= User.valid_login?(params[:username],params[:password])
      if !user.approved
        render json: {message: (params[:lang] == 0 ? "Колдонуучу тастыкталган эмес, жаңыдан катталыңыз!" : "Пользователь не подтвержден, пройдите регистрацию заново!")},status:401 and return 
      end

      clearRedis(user)
      #      PasswordResetMailer.password_reset(user).deliver
      allow_token_to_be_used_only_once_for(user)
      send_auth_token_for_valid_login_of(user)
    else
      render json: {message: (params[:lang]==0 ? "Берилген тел.номур же пароль туура эмес" : "Не верный логин или пароль!")},status:401
    end
  end
  def change_password
    user = User.find_by(username:params[:username])
    render json: {message: (params[:lang]==0 ? "Берилген тел.номур боюнча колдонуучу табылган жок" : "Пользователь с таким номером не существует")},status:501 and return if !user

    check_result = Sms.check(params[:username])
    render json: {message: (params[:lang]==0 ? "тел.номур туура эмес же катталган эмес" : "Неверный номер телефона или номер не существует")},status:501 and return if check_result["response"]["status"]!="0"
    
    user.pin = rand(1000..9999).to_s 
    user.save
    
    sms_result = Sms.send_sms(user.username, params[:lang]==0 ? "#{user.pin} - секретный код для обновления пароля" : "#{user.pin} - Жашыруун код")
    render json: {username:user.username}
  end

  def approve
    user = User.find_by(username:params[:username])
    render json: {message: (params[:lang]==0 ? "Берилген тел.номур боюнча колдонуучу табылган жок" : "Пользователь с таким именем не существует")},status:501 and return if !user
    render json: {message: (params[:lang]==0 ? "Жашыруун код туура эмес" : "Секретный код не верный")},status:501 and return if user.pin!=params[:pin]

    user.password = params[:password]
    user.token_created_at = DateTime.now
    user.regenerate_token
    send_auth_token_for_valid_login_of(user)
  end

  def destroy
    logout
    render json: {message: "Сессия закрыт"},status:200
  end

  private 
  def send_auth_token_for_valid_login_of(user)
    payments = user.payments.sum(:summa)
    render json: {
      token: user.token,
      token_created_at: user.token_created_at, 
      user_id:user.id,
      username:user.username,
      fullname: user.fullname,
      payments: payments,
      full_access: user.full_access,
      aggree: user.aggree,
      lang: user.lang
    }
  end
  def allow_token_to_be_used_only_once_for(user)
    user.token_created_at = DateTime.now
    user.regenerate_token
    redis = Redis.new
    token = redis.get("user:#{user.id}:token")
    redis.del("token:#{token}") if token

    redis.set("user:#{user.id}:token",user.token)
    redis.set("token:#{user.token}",user.id)
  end
  def clearRedis(user)
    redis = Redis.new
    redis.keys("sc:#{user.token}:*").each do |key|
      redis.del(key)
    end
  end

  def logout
    clearRedis(current_user)
    current_user.invalidate_token
  end 
end
