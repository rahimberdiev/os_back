class SignoutController < ApplicationController
  def create
    user = User.find_by(username:params[:username])
    render json: {message: (params[:lang]==0 ? "Берилген тел.номур буга чейин катталган" : "Пользователь с таким номером тел. уже существует")},status:501 and return if (user && user.approved)

    check_result = Sms.check(params[:username])
    render json: {message: (params[:lang]==0 ? "Тел.номур туура эмес же катталган эмес" : "Неверный номер телефона или номер не существует")},status:501 and return if check_result["response"]["status"]!="0"
    
    user = User.new if !user
    user.username = params[:username]
    user.fullname = params[:fullname]
    user.region = params[:region]
    user.district = params[:city]
    user.lang = params[:lang]
    user.password=params[:password]
    user.full_access = false
    user.aggree = params[:agreed]
    user.pin = rand(1000..9999).to_s 
    user.save
    sms_result = Sms.send_sms(user.username, (params[:lang]==0 ? "#{user.pin} - Системага каттоо үчүн жашыруун код" : "#{user.pin} - секретный код для подтверждения регистрации"))
    render json: {username:user.username, fullname:user.fullname}
  end
  def approve
    user = User.find_by(username:params[:username])
    render json: {message: (params[:lang]==0 ? "Берилген тел.номур табылган жок же буга чейин катталган" : "Пользователь с таким именем не существует либо уже зарестрирован")},status:501 and return if (!user || user.approved)
    render json: {message: (params[:lang]==0 ? "Туура эмес жашыруун код берилди" : "Не верный секретный код")},status:501 and return if user.pin!=params[:code]

    user.approved = true
    user.token_created_at = DateTime.now
    user.regenerate_token
    render json: {
      token: user.token,
      token_created_at: user.token_created_at, 
      user_id:user.id,
      username:user.username,
      fullname: user.fullname,
      aggree: user.aggree,
      city: user.district,
      region:user.region,
      payments: 0,
      full_access: user.full_access
    }
  end
end
