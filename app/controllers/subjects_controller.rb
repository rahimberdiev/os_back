class SubjectsController < SecureController
  def list
    subjects = Subject.all.where(lang:current_user.lang).order(:no)
    render json: subjects.map{|subject| {
      id:subject.id,
      no: subject.no,
      lang: subject.lang,
      name: subject.name,
      description:subject.description,
      logo:subject.logo.url(:thumb)}}
  end
end
