class Exam < ApplicationRecord
  belongs_to :section
  has_many :answers
  has_many :pics
end
