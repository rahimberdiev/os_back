class Prob < ApplicationRecord
  has_attached_file :img, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :img, content_type: /\Aimage\/.*\z/
  belongs_to :testing
  has_many :opts, dependent: :destroy

end
