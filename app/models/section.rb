class Section < ApplicationRecord
  belongs_to :course
  has_many :screencasts
  has_many :exams
end
