class User < ApplicationRecord
  has_attached_file :photo, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\z/

  has_secure_password
  has_secure_token

  has_many :userCourses
  has_many :payments
  has_many :courseProgresses,:dependent => :destroy
  has_many :sectionProgresses,:dependent => :destroy

  def invalidate_token
    self.update_columns(token: nil,token_created_at: nil)
  end

  def self.valid_login?(username,password)
    user = find_by(username: username)
    user&&user.authenticate(password)
  end
end
