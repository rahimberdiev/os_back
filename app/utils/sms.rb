class Sms 
  def self.send_sms(phone,message) 
    norm_phone=   '+996'+phone.last(9)
    uri = URI.parse('https://smspro.nikita.kg/api/message')
    msg = Message.create(phone:norm_phone,body:message)
    header = {'Content-Type': 'text/xml'}
    message = {
      login: "apgradekg",
      pwd: "CXS7Yf9i",
      id: "ap#{msg.id}",
      sender: "apgrade.kg",
      text: message,
      phones:{
        phone: norm_phone
      },
      test:"0"
    }
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    request = Net::HTTP::Post.new(uri.request_uri, header)
    request.body = message.to_xml(root: :message)
    response = http.request(request)
    #json = {"response"=>{"status"=>"0"}}
    json = JSON.parse(Hash.from_xml(response.body).to_json)
    msg.status = json["response"]["status"]
    msg.save
    return json
  end
  def self.check(phone) 
    norm_phone=   '+996'+phone.last(9)
    uri = URI.parse('https://smspro.nikita.kg/api/def')
    header = {'Content-Type': 'text/xml'}
    message = {
      login: "apgradekg",
      pwd: "CXS7Yf9i",
      phone: norm_phone
    }
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    request = Net::HTTP::Post.new(uri.request_uri, header)
    request.body = message.to_xml(root: :def).gsub("<def>","<def xmlns='http://Giper.mobi/schema/PhoneDEF'>")
    response = http.request(request)
    return JSON.parse(Hash.from_xml(response.body).to_json)
  end
end
