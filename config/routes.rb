Rails.application.routes.draw do
  scope '/api', constraints: { format: 'json' } do
    get 'profile/read'

    get 'profile/write'
    post 'profile/save_avatar'

    post 'sessions/create'
    post 'sessions/refresh_session'
    post 'sessions/change_password'
    post 'sessions/approve'
    delete 'sessions/destroy'

    post 'signout/create'
    post 'signout/approve'

    post 'subjects/list'
    post 'courses/list'

    post 'quizzes/list'
    post 'quizzes/check'
    post 'exams/list'
    post 'exams/check'

    post 'probs/probs_list'
    post 'probs/list'
    post 'probs/check'

    post 'sections/list'
    post 'sections/move'

    post 'screencasts/list'
    post 'screencasts/activate'

    post 'regions/list'
  end
end
